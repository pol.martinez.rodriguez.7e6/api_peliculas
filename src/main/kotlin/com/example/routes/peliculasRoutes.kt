package com.example.routes

import com.example.model.Pelicula
import com.example.model.peliculasStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.peliculasRouting() {
    route("/peliculas"){
        get  {
            if (peliculasStorage.isNotEmpty()) call.respond(peliculasStorage.values)
            else call.respondText("No hi han pel·liculas", status = HttpStatusCode.OK)
        }
        get("{id?}"){
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText("Missing id", status = HttpStatusCode.BadRequest)
            if (peliculasStorage.isNotEmpty()){
                val id = call.parameters["id"]
                if (peliculasStorage[id] != null) return@get call.respond(peliculasStorage[id]!!)
                return@get call.respondText("No film with id $id", status = HttpStatusCode.NotFound)
            }
            return@get call.respondText ("No hi han pel·liculas", status = HttpStatusCode.NotFound)
        }
        post{
            val pelicula = call.receive<Pelicula>()
            peliculasStorage[pelicula.id] = pelicula
            call.respondText("Film stored correctly", status = HttpStatusCode.Created)
        }
        delete("{id?}"){
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText("Missing id", status = HttpStatusCode.BadRequest)
            if (peliculasStorage.isNotEmpty()){
                val id = call.parameters["id"]
                peliculasStorage[id!!] = null
                return@delete call.respondText("La pelicula ha sigut esborrada correctament", status = HttpStatusCode.Accepted)
            }
            return@delete call.respondText("No hi han pel·liculas", status = HttpStatusCode.NotFound)
        }
        put ("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]
            if (peliculasStorage[id]!= null){ peliculasStorage[id!!] = call.receive<Pelicula>(); return@put call.respondText("Film changed correctly", status = HttpStatusCode.OK)}
            call.respondText("Film with id \"$id\" can't be changed or not found ", status = HttpStatusCode.NotFound)
        }
    }
}