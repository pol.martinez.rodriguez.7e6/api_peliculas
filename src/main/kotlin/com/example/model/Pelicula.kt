package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Pelicula (
    val id : String,
    val titol : String,
    val any : String,
    val genere : String,
    val director : String,
    val comentaris : MutableList<Comentari>
)
val peliculasStorage = mutableMapOf<String, Pelicula?>()