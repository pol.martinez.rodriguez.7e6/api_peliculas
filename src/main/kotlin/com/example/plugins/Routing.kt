package com.example.plugins

import com.example.routes.comentatisRouting
import com.example.routes.peliculasRouting
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        comentatisRouting()
        peliculasRouting()
    }
}
