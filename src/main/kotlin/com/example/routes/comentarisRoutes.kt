package com.example.routes

import com.example.model.Comentari
import com.example.model.peliculasStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.comentatisRouting() {
    route("/peliculas/{idPelicula?}/comentaris") {
        post{
            if (call.parameters["idPelicula"].isNullOrBlank()) return@post call.respondText("Missing id", status = HttpStatusCode.BadRequest)
            if (peliculasStorage.isNotEmpty()){
                val id = call.parameters["idPelicula"]
                if (peliculasStorage[id] != null){ peliculasStorage[id!!]!!.comentaris.add(call.receive<Comentari>()) ; return@post call.respondText("Comentari guardat Correctament", status = HttpStatusCode.Created)}
                return@post call.respondText("No hi ha cap pelicula amb l'id $id", status = HttpStatusCode.NotFound)
            }
            call.respondText("No hi ha cap pelicula", status = HttpStatusCode.NotFound)
        }
        get {
            if (call.parameters["idPelicula"].isNullOrBlank()) return@get call.respondText("Missing id", status = HttpStatusCode.BadRequest)
            if (peliculasStorage.isNotEmpty()){
                val id = call.parameters["idPelicula"]
                if (peliculasStorage[id] != null) return@get call.respond(peliculasStorage[id]!!.comentaris)
                return@get call.respondText("No hi han comentaris sobre la pelicula amb l'id \"$id\"")
            }
            call.respondText("No hi ha cap pelicula", status = HttpStatusCode.OK)
        }
    }
}