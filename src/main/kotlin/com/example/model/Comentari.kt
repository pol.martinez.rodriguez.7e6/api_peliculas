package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Comentari(
    val id: String,
    val idPelicula: String,
    val comentari: String,
    val dataCreacio : String
)